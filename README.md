# My UserChrome

## Installation

Clone this repository directly into `your-ff-profile/chrome` or just download [userChrome.css](userChrome.css) and place it there. More info [here](https://www.reddit.com/r/FirefoxCSS/comments/73dvty/tutorial_how_to_create_and_livedebug_userchromecss/).

There is a number you'll need to change in the file if on Windows and some fiddling will be required if your title bar buttons are on the left (e.g. on Mac).

## DuckDuckGo style

Install the DuckDuckGo style and receive automatic updates:
[![Stylus badge](https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59.svg?longCache=true&style=flat)](<https://gitlab.com/markonius/my-round-userchrome/raw/master/DuckDuckGo.user.styl>)
